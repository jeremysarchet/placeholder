# Html fragments to be used for a series of pages
# Generate a listing of links to pages
# For a given page, links to jump to the previous and next in the series

require "fileutils"
require "pathname"

module Series
	extend self

	def generate_listing(path)
		directory = Pathname.new(File.dirname(path))
		subdirectories = directory.children.select { |child| child.directory? }

		result = "<ul class=\"listing\">"
		subdirectories.each do |subdirectory|
			entry = File.basename(subdirectory)

			# Read the source file and get its metadata
			entry_path = File.join(directory, entry, "index.html")
			document = File.read(entry_path)
			meta, content = Metadata.split_and_parse_front_matter(document)

			# Get handles to the needed metadata items
			title = meta["title"] || "Untitled"
			date = meta["date"] || ""

			# Generate the listing entry
			result << "<li>
			<a href=\"#{entry}/%{postfix}\">#{title}</a><br>
			#{date}
			</li>"
		end
		result << "</ul>"
		return result
	end

	def generate_jumps(path)
		directory = Pathname.new(File.dirname(File.dirname(path)))
		subdirectories = directory.children.select { |child| child.directory? }
		names = subdirectories.map { |d| File.basename(d)}
		this = File.basename(File.dirname(path))

		i = names.index(this)
		previous_name = i == 0 ? nil : names[i - 1]
		next_name = i == names.size - 1 ? nil : names[i + 1]

		result = ""
		if names.size > 1
			if i == 0
				result << "<a href=\"../#{next_name}/%{postfix}\">Next</a>"
			elsif i == names.size - 1
				result << "<a href=\"../#{previous_name}/%{postfix}\">Previous</a>"
			else
				result << "<a href=\"../#{previous_name}/%{postfix}\">Previous</a>
				<span>  |  </span>
				<a href=\"../#{next_name}/%{postfix}\">Next</a>"
			end
		end

		return result
	end

end
