# Started on 2019-06-11
# My very own static site generator by Jeremy Sarchet

require "fileutils"
require "find"
require "pathname"
require "set"
require "yaml"

require "htmlbeautifier"

require_relative "metadata"
require_relative "series"

# For serving a default file when requesting a directory in different contexts
LOCAL_FILESYSTEM_POSTFIX = "index.html"
HTTP_SERVER_POSTFIX = ""
POSTFIX = LOCAL_FILESYSTEM_POSTFIX

# Pattern for yaml front matter in html files
# The front matter is used by the site generator and stripped away
FRONT_MATTER_PATTERN = /---[\s\S]*?---\n/

# Pattern for substitution of fragments
INTERPOLATION_PATTERN = /%{[\S]*?}/

# Directories and their names for site generation
SOURCE = "./source" # the raw website content
FRAGMENTS = "./fragments" # common elements to be filled in via templating
WRAPPERS = "./wrappers" # makes a complete html document when content inserted
BUILD = "./build" # where the generated static website is output

# NOTE: There is not any sort of cleanup of the build directory.
# If the source contents and/or generation procedure changes to as to
# "orphan" anything in the build directory, then those orphan(s) should
# be deleted manually, or the entire contents of the build directory
# should be deleted, and the whole thing updated anew.

def make_way(path)
	# Preparation for file overwrite. Creates intermediate directories as
	# needed, and deletes the file if it exists.
	FileUtils.makedirs(File.dirname(path))
	File.delete(path) if File.exist?(path)
end

def update(src, dst)
	# Copies a file from src to dst paths, overwriting, and creating
	# intermediate directories as needed
	make_way(dst)
	FileUtils.copy_file(src, dst)
	puts "Updated:    #{src}\n"
end

def resolve(name, meta)
	fragment_path = "#{FRAGMENTS}/#{name}.html"
	if name == "listing"
		result = Series.generate_listing(meta["path"])
	elsif name == "jumps"
		result = Series.generate_jumps(meta["path"])
	elsif meta.key?(name)
		result = meta[name]
	elsif File.file?(fragment_path)
		result = File.read(fragment_path)
	else
		result = "UNRESOLVED FRAGMENT: `#{name}`"
		warn(result)
	end
	return result.to_s
end

def unresolved?(document)
  return INTERPOLATION_PATTERN.match?(document.to_s)
end

def resolve_recursive(document, meta)
	if unresolved?(document)
		placeholders = document.scan(INTERPOLATION_PATTERN)
		placeholders = Set.new(placeholders)  # unsure why this is necessary

		placeholders.each do |placeholder|
			name = document[INTERPOLATION_PATTERN][2..-2]
			fragment = resolve(name, meta)
			resolved_fragment = resolve_recursive(fragment, meta)
			document = document.gsub(/%{#{name}}/, resolved_fragment)
		end
	end
	return document
end

def apply_wrapping_recursive(document, wrapping="base")
	meta, content = Metadata.split_and_parse_front_matter(document)

	if meta.key?("wrapping")
		name = meta["wrapping"]
		_, content = apply_wrapping_recursive(content, name)
	end

	wrapper = File.read("#{WRAPPERS}/#{wrapping}.html")
	return meta, wrapper.gsub(/%{content}/, content)
end

def upgrade_html(source_file_path, build_file_path)
	puts "Generating: #{build_file_path}"

	source_file_dir = Pathname.new(source_file_path).dirname
	path_to_root = Pathname.new(SOURCE).relative_path_from(source_file_dir).to_s

	# Read the file. Done only once per source file
	document = File.read(source_file_path)

	# Insert the content into wrapping recursively
	meta, wrapped_content = apply_wrapping_recursive(document)

	# Add in some extra metadata
	meta.merge!( {"path" => source_file_path.to_s,
								"postfix" => POSTFIX,
								"root" => path_to_root,
								} )
	# Apply the filling to the content and continue recursively
	output = resolve_recursive(wrapped_content, meta)

	# Output the upgraded file to the build directory
	make_way(build_file_path)
	new_file = File.open(build_file_path, "w")
	new_file.puts(HtmlBeautifier.beautify(output))
	new_file.close

end


# Walk the source filetree and get all the non-hidden files
file_paths = Find.find(SOURCE).reject{
	|path| File.directory?(path) || File.basename(path)[0] == "."
}

# Update the build filetree
file_paths.each do |file_path|
	build_file_path = Pathname.new(file_path).sub(SOURCE, BUILD)

	# If it's an html file, update it every time. This is a lightweight
	# operation anyways. Decision is that it's not worth it and doing
	# a check will be tricky as these files are modified (templating is
	# applied) in the process of site generation
	if file_path.match(/\.html\Z/)
		# Apply templates
		upgrade_html(file_path, build_file_path)

	# If it's not an html file, check before updating it in the build
	# to save the work of overwriting large assets unnecessarily
	else
		# Has the source file been modified more recently than the build file?
		unless FileUtils.uptodate?(build_file_path, [file_path])
			# If so, update the build file
			update(file_path, build_file_path)
		end
	end
end
