# Placeholder

My very own static site generator.

Placeholder is a simple static site generator built with plain Ruby and designed to suit the particulars of my own website [www.jeremysarchet.com](www.jeremysarchet.com)

## Core concepts:
* It takes a source filetree and outputs a mirrored filetree which is the completed website.
* It goes through each source page and applies templating logic and outputs the completed page at the mirrored path.
* Pages are html files prefixed with meta information in YAML syntax and containing placeholders of the form `%{substitute-me}` to be used for substitution.
* It takes a page and applies "wrapping", to enclose it for presentation as a complete html document.
* It also takes a page and applies "filling", via substitution of placeholders.
* Both wrapping and filling can be nested.
* Filling can be html fragments, or it can be generated html, or it can be meta information from the page's front matter.
* Before they are output, the completed html pages are pretty printed using the [HTML Beautifier](https://github.com/threedaymonk/htmlbeautifier) gem.
* Relative paths are supported so that the generated site can be run from a web server or from the local file system.


NOTE: There is not any sort of cleanup of the build directory.
If the source contents and/or generation procedure changes to as to
"orphan" anything in the build directory, then those orphan(s) should
be deleted manually, or the entire contents of the build directory
should be deleted, and the whole thing updated anew.


